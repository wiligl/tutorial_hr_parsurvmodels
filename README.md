# README #

After installing the required R and LaTeX packages you can compile the document in the R console using library("knitr"); knit2pdf("Tutorial_ParamSurvModels.Rnw")

### What is this repository for? ###

* A tutorial on how to calculate (average) hazard ratios for parametric survival models based on the R/flexsurv package.
* Latest version


### Disclaimer ###

This program (Program is used here in the sense of literate
  programming, which combines natural language and computer language
  in an integrated program or document, respectively.)  is free
  software. You can redistribute it and/or modify it under the terms
  of the GNU General Public License as published by the Free Software
  Foundation;

This program is distributed in the hope that it will be useful, but
without any warranty, without even the implied warranty of
merchantability or fitness for a particular purpose.  See the GNU
General PublicLicense for more details. You can retrieve a copy of the
GNU public licence version 3 online (https://www.gnu.org/licenses/gpl-3.0.en.html).

### Who do I talk to? ###

* Author: Wilmar Igl
